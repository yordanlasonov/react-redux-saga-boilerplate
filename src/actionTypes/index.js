export const ADD_COUNTER = 'ADD_COUNTER'

export const MIN_COUNTER = 'MIN_COUNTER'

export const MUL_COUNTER = 'MUL_COUNTER'

export const INCREMENT_ASYNC = 'INCREMENT_ASYNC'

export const RESET_COUNTER = 'RESET_COUNTER'

//actions describe what happened