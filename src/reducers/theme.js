import { initialState } from '../initialState';
import { ADD_COUNTER, MIN_COUNTER, MUL_COUNTER, RESET_COUNTER } from '../actionTypes';

function counterApp(state = initialState, action) {
	switch (action.type) {
		case ADD_COUNTER:
			return {
				counter: state.counter + 1
			};
		case MIN_COUNTER:
			return {
				counter: state.counter - 1
			};
		case MUL_COUNTER:
			return {
				counter: state.counter * 2
			};
		case RESET_COUNTER:
			return { counter: 100 };
		default:
			return state;
	}
}

export default counterApp;
